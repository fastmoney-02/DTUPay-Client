package test_utility;

import java.util.Random;

import bank_client.model.User;

public final class TestHelper {
	public static User GenerateRandomUser()
	{
		Random rng = new Random();
		int ranInt = rng.nextInt();
		
		User user = new User(Integer.toString(ranInt), "02Test", "TestAgent");
		return user;
	}
}
