package jUinitTests;

import org.junit.Test;

import static org.junit.Assert.assertNotNull;

import javax.ws.rs.NotFoundException;

import bank_client.BankClient;
import bank_client.model.AccountCreationRequest;
import bank_client.model.User;

public class UserCreationTest {
	
	@Test(expected = NotFoundException.class)
	public void createDeleteUser(){
		BankClient bankClient = new BankClient("http://fastmoney-00.compute.dtu.dk:80/rest/");
		AccountCreationRequest user = new AccountCreationRequest(10, new User("1910948978","John","Doe"));
		String accountId = bankClient.createAccount(user);
		assertNotNull(bankClient.getAccount(accountId));
		bankClient.deleteAccount(accountId);
		bankClient.getAccount(accountId);
	}


	
}
