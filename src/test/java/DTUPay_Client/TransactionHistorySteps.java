package DTUPay_Client;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import DTUPayClient.DTUPayClient;
import bank_client.BankClient;
import bank_client.model.AccountCreationRequest;
import bank_client.model.User;
import dataTransferObject.CustomerDTO;
import dataTransferObject.MerchantDTO;
import dataTransferObject.TransactionDTO;
import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import test_utility.TestHelper;

public class TransactionHistorySteps {

	BankClient bankClient = new BankClient("http://fastmoney-00.compute.dtu.dk:80/rest/");
	DTUPayClient dtuPayClient = new DTUPayClient("http://fastmoney-02.compute.dtu.dk:8080/");
	
	User customerUser = TestHelper.GenerateRandomUser();
	User merchantUser = TestHelper.GenerateRandomUser();
	
	String customerAccountId;
	String merchantAccountId;
	
	String[] tokens;
	CustomerDTO customer;
	MerchantDTO merchant;
	
	TransactionDTO[] transactions;

	@Given("The customer has a bank account with a balance of {int} credits")
	public void theCustomerHasABankAccountWithABalanceOfCredits(Integer int1) {
		customerAccountId = bankClient.createAccount(new AccountCreationRequest(int1, customerUser));
	}
	
	@Given("the merchant has a bank account with a balance of {int} credits")
	public void theMerchantHasABankAccountWithABalanceOfCredits(Integer int1) {
		merchantAccountId = bankClient.createAccount(new AccountCreationRequest(int1, merchantUser));
	}

	@Given("the customer is registered at DTUPay")
	public void theCustomerIsRegisteredAtDTUPay() {
		customer = dtuPayClient.createCustomer(customerAccountId);
	}

	@Given("the merchant is registererd at DTUPay")
	public void theMerchantIsRegistererdAtDTUPay() {
		merchant = dtuPayClient.createMerchant(merchantAccountId);
	}
	
	@Given("the customer has {int} token")
	public void theCustomerHasToken(Integer int1) {
		tokens = dtuPayClient.requestTokens(customer.getId(), int1);
	}

	@Given("the customer has transfered {int} credits to the merchant")
	public void theCustomerHasTransferedCreditsToTheMerchant(Integer int1) {
		dtuPayClient.makePayment(merchant.getId(), new BigDecimal(int1), tokens[0]);
	}

	@When("the customer requests their transaction history")
	public void theCustomerRequestsTheirTransactionHistory() {
		transactions = dtuPayClient.requestCustomerTransactionHistory(customer.getId());
	}

	@Then("a transaction shows on their history")
	public void aTransactionShowsOnTheirHistory() {
	    assertTrue(transactions.length > 0);
	}

	@Then("the token used is the same as the customer used")
	public void theTokenUsedIsTheSameAsTheCustomerUsed() {
		assertEquals(tokens[0], transactions[0].getTokenId());
	}	

	@Then("the amount of the transaction is {int}")
	public void theAmountOfTheTransactionIs(Integer int1) {
		assertEquals(new BigDecimal(int1), transactions[0].getAmount());
	}

	@Then("the reciever is the intended merchant")
	public void theRecieverIsTheIntendedMerchant() {
		assertEquals(merchant.getId(), transactions[0].getMerchantId());
	}
	
	@Then("the status of the transaction is success")
	public void theStatusIsSuccess(){
		assertTrue(transactions[0].isSuccess());
	}

	@When("the merchant requests their transaction history")
	public void theMerchantRequestsTheirTransactionHistory() {
		transactions = dtuPayClient.requestMerchantTransactionHistory(merchant.getId());
	}
	
	@After("@TransactionHistory")
	public void cleanUp() {
		bankClient.deleteAccount(customerAccountId);
		bankClient.deleteAccount(merchantAccountId);
	}
}
