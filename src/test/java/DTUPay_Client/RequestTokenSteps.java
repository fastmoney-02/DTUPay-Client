package DTUPay_Client;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;

import DTUPayClient.DTUPayClient;
import dataTransferObject.CustomerDTO;

public class RequestTokenSteps {
	CustomerDTO user;
	CustomerDTO otherUser;
	ArrayList<String> tokens;
	DTUPayClient dtuPayClient = new DTUPayClient("http://fastmoney-02.compute.dtu.dk:8080/");
	
	@Given("a user exists")
	public void aUserExists() {
		user = dtuPayClient.createCustomer("1234");
	}

	@Given("the user has {int} tokens")
	public void theUserHasTokens(Integer int1) {
		 tokens = new ArrayList<String>(Arrays.asList(dtuPayClient.requestTokens(user.getId(), int1)));
	}

	@When("the user requests {int} token\\(s)")
	public void theUserRequestsTokenS(Integer int1) {
		tokens.addAll(Arrays.asList(dtuPayClient.requestTokens(user.getId(), int1)));
	}

	@Then("the user has {int} token\\(s)")
	public void theUserHasTokenS(Integer int1) {
		assertEquals(int1.intValue(), tokens.size());
	}
}
