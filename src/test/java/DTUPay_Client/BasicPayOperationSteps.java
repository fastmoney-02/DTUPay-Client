package DTUPay_Client;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.Random;

import javax.ws.rs.NotFoundException;

import DTUPayClient.DTUPayClient;
import bank_client.BankClient;
import bank_client.model.Account;
import bank_client.model.AccountCreationRequest;
import bank_client.model.User;
import dataTransferObject.CustomerDTO;
import dataTransferObject.MerchantDTO;
import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import test_utility.TestHelper;

public class BasicPayOperationSteps {
	
	BankClient bankClient = new BankClient("http://fastmoney-00.compute.dtu.dk:80/rest/");
	DTUPayClient dtuPayClient = new DTUPayClient("http://fastmoney-02.compute.dtu.dk:8080/");
	User customerUser = TestHelper.GenerateRandomUser();
	User merchantUser = TestHelper.GenerateRandomUser();
	
	String customerAccountId;
	String merchantAccountId;
	String merchantId;
	String customerId;
	
	CustomerDTO customer;
	MerchantDTO merchant;
	
	String token;
	String[] tokens;
	
	BigDecimal initialBalanceCustomer;
	BigDecimal initialBalanceMerchant;
	
	Exception exception;
	
	boolean paymentSucceeded;
	
	@Given("the customer has bank account with {int} credits")
	public void theCustomerHasBankAccountWithCredits(Integer balance) {
		customerAccountId = bankClient.createAccount(new AccountCreationRequest(balance, customerUser));
		assertNotNull(customerAccountId);
		initialBalanceCustomer = bankClient.getAccount(customerAccountId).getBalance();
		assertEquals(balance.intValue(), initialBalanceCustomer.intValue());
	}

	@Given("the customer is registered with DTUPay")
	public void theCustomerIsRegisteredWithDTUPay() {
	    customer = dtuPayClient.createCustomer(customerAccountId);
	    customerId = customer.getId();
	    assertNotNull(customer);
	}

	@Given("that the customer has {int} token")
	public void thatTheCustomerHasToken(Integer amount) {
		tokens = dtuPayClient.requestTokens(customer.getId(), amount);
		assertEquals((long)amount, tokens.length);
	}
	
	@Given("that the customer has a fake token")
	public void thatTheCustomerHasAFakeToken() {
	    token = "fakeToken";
	    assertNotNull(token);
	}
	
	@Given("the merchant has a bank account with {int} credits")
	public void theMerchantHasABankAccountWithCredits(Integer balance) {
		merchantAccountId = bankClient.createAccount(new AccountCreationRequest(balance, merchantUser));
		assertNotNull(merchantAccountId);
		initialBalanceMerchant = bankClient.getAccount(merchantAccountId).getBalance();
		assertEquals(balance.intValue(), initialBalanceMerchant.intValue());
	}

	@Given("the merchant is registered with DTUPay")
	public void theMerchantIsRegisteredWithDTUPay() {
		merchant = dtuPayClient.createMerchant(merchantAccountId);
		merchantId = merchant.getId();
		assertNotNull(merchant);
	}
	
	@When("the merchant request a payment for {int} credits")
	public void theMerchantRequestAPaymentForCredits(Integer int1) {
		if(token != null) {
			paymentSucceeded = dtuPayClient.makePayment(merchantId, BigDecimal.valueOf(int1), token);
		} 
		else {
			if(tokens.length > 0) {
				token = tokens[0];
				assertNotNull(token);
			}
			else {
				token = null;
			}
		paymentSucceeded = dtuPayClient.makePayment(merchantId, BigDecimal.valueOf(int1), token);
		}
	}

	@Then("{int} credits are transfered from the customer bank account to the merchant bank account")
	public void creditsAreTransferedFromTheCustomerBankAccountToTheMerchantBankAccount(Integer int1) {
		BigDecimal balanceCustomer = bankClient.getAccount(customerAccountId).getBalance();
		BigDecimal balanceMerchant = bankClient.getAccount(merchantAccountId).getBalance();
		
		if(int1 > 0)
			assertTrue(paymentSucceeded);
		
		assertEquals(balanceCustomer, initialBalanceCustomer.subtract(BigDecimal.valueOf(int1)));
		assertEquals(balanceMerchant, initialBalanceMerchant.add(BigDecimal.valueOf(int1)));
		initialBalanceCustomer = balanceCustomer;
		initialBalanceMerchant = balanceMerchant;
	}
	
	private boolean deleteBankAccount(String cpr)
	{
		try
		{
			Account acc = bankClient.getAccount(cpr);
			bankClient.deleteAccount(acc.getId());
			return true;
		}
		catch(NotFoundException e)
		{
			return true;
		}
	}
	
	@After("@BankAccounts")
	public void teardownBankAccount()
	{
		deleteBankAccount(customerUser.cprNumber);
		deleteBankAccount(merchantUser.cprNumber);
	}
	@Given("the merchant has invalid bank account")
	public void theMerchantHasInvalidBankAccount() {
		merchantAccountId = "InvalidAccountoaijdf";
	}
	
	@Then("{int} credits are transfered from the customer bank account")
	public void creditsAreTransferedFromTheCustomerBankAccount(Integer int1) {
		BigDecimal balanceCustomer = bankClient.getAccount(customerAccountId).getBalance();
		assertEquals(initialBalanceCustomer.subtract(BigDecimal.valueOf(int1)), balanceCustomer);
	}
	@Given("the merchant is not registered with DTUPay")
	public void theMerchantIsNotRegisteredWithDTUPay() {
		merchant = new MerchantDTO("UnknownMerchant", "UnknownMerchant");
		merchantId = merchant.getAccountId();
		assertNotNull(merchant);
	}
}
