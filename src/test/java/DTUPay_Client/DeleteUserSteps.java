package DTUPay_Client;

import static org.junit.Assert.*;

import DTUPayClient.DTUPayClient;
import dataTransferObject.CustomerDTO;
import dataTransferObject.MerchantDTO;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class DeleteUserSteps {
	
	CustomerDTO customer;
	MerchantDTO merchant;

	DTUPayClient dtuPayClient = new DTUPayClient("http://fastmoney-02.compute.dtu.dk:8080/");
	
	boolean result;
	
	@Given("a customer exists")
	public void aCustomerExists() {
	    customer = dtuPayClient.createCustomer("test");
	    assertNotNull(customer);
	}

	@When("the customer deletes their account")
	public void theCustomerDeletesTheirAccount() {
	    result = dtuPayClient.deleteCustomer(customer.getId());
	}

	@Then("the customers account is deleted")
	public void theCustomersAccountIsDeleted() {
		assertTrue(result);
	    customer = dtuPayClient.getCustomer(customer.getId());
	    assertNull(customer);
	}

	@Given("a merchant exists")
	public void aMerchantExists() {
	    merchant = dtuPayClient.createMerchant("test");
	    assertNotNull(merchant);
	}

	@When("the merchant deletes their account")
	public void theMerchantDeletesTheirAccount() {
		result = dtuPayClient.deleteMerchant(merchant.getId());
	}

	@Then("the merchant account is deleted")
	public void theMerchantAccountIsDeleted() {
		assertTrue(result);
		merchant = dtuPayClient.getMerchant(merchant.getId());
	    assertNull(merchant);
	}

	@Given("a customer does not exists")
	public void aCustomerDoesNotExists() {
	    customer = new CustomerDTO("Beep", "Boop");
	}

	@Then("the deletion is rejected")
	public void theDeletionIsRejected() {
	    assertFalse(result);
	}

	@Given("a merchant does not exists")
	public void aMerchantDoesNotExists() {
		merchant = new MerchantDTO("Beep", "Boop");
	}
}
