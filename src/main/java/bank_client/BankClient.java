package bank_client;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import bank_client.model.Account;
import bank_client.model.AccountCreationRequest;

public class BankClient {
	Client c = ClientBuilder.newClient();
	WebTarget bankTarget;
	
	public BankClient(String targetUrl) {
		bankTarget = c.target(targetUrl);
	}
	
	
	public String createAccount(AccountCreationRequest customer)
	{
		String newAccountId = bankTarget
			.path("accounts")
			.request(MediaType.TEXT_PLAIN)
			.post(Entity.json(customer), String.class);	

		return newAccountId;
	}
	
	public boolean deleteAccount(String accountId)
	{
		Response response = bankTarget.path("accounts").path(accountId).request().delete();
		
		return response.getStatus() == 204;
	}
	
	public Account getAccount(String accountId) {
		return bankTarget.path("accounts").path(accountId).request().get(Account.class);
	}
	
	public Account getAccountByCpr(String cpr) {
		return bankTarget.path("accounts").path("cpr").path(cpr).request().get(Account.class);
	}
}
