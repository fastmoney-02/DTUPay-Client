package bank_client.model;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Transaction {

 		public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public String getCreditor() {
		return creditor;
	}

	public void setCreditor(String creditor) {
		this.creditor = creditor;
	}

	public String getDebtor() {
		return debtor;
	}

	public void setDebtor(String debtor) {
		this.debtor = debtor;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

		BigDecimal amount;//	number
 		BigDecimal balance;//	number
 		String creditor;//	string
 		String debtor; //	string
 		String description;//	string
 		int id;//	integer
 		Date time;
 		
 		public Transaction() {}

		public Transaction(BigDecimal amount, BigDecimal balance, String creditor, String debtor, String description,
				int id, Date time) {
			super();
			this.amount = amount;
			this.balance = balance;
			this.creditor = creditor;
			this.debtor = debtor;
			this.description = description;
			this.id = id;
			this.time = time;
		}
 		
 		
 		
 		
}
