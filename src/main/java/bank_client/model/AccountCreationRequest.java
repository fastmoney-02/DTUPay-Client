package bank_client.model;

public class AccountCreationRequest {
	public int balance; 
	public User user;
	

	public AccountCreationRequest(int balance, User user) {
		super();
		this.balance = balance;
		this.user = user;
	}
}
