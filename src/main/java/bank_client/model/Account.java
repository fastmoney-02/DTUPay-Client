package bank_client.model;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Account {
	
	private BigDecimal balance;
	private String id;
	private Transaction[] transactions;
	private User user;
	
	public Account(){
		
	}
	
	public Account(BigDecimal balance, String id, Transaction[] transactions, User user) {
		this.balance = balance;
		this.id = id;
		this.transactions = transactions;
		this.user = user;
	}
	public String getId()
	{
		return id;
	}
	public BigDecimal getBalance() {
		return balance;
	}
	public Transaction[] getTransactions() {
		return transactions;
	}
	public User getUser() {
		return user;
	}
}
