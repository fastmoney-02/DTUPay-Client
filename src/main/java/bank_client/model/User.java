package bank_client.model;

public class User {
	public String cprNumber; 
	public String firstName; 
	public String lastName;
	
	public User(){
		
	}
	
	public User(String cpr, String firstName, String lastName) {
		this.cprNumber = cpr;
		this.firstName = firstName;
		this.lastName = lastName;
	}
}
