package DTUPayClient;

import java.math.BigDecimal;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import dataTransferObject.CustomerDTO;
import dataTransferObject.MerchantDTO;
import dataTransferObject.PaymentArgs;
import dataTransferObject.RequestTokensArgs;
import dataTransferObject.TransactionDTO;

public class DTUPayClient {
	private final String PAYMENT_ENDPOINT = "payments";
	
	Client c = ClientBuilder.newClient();
	WebTarget target;
	WebTarget merchantTarget;
	
	public DTUPayClient(String targetUrl) {
		target = c.target(targetUrl);
		merchantTarget = c.target("http://fastmoney-02.compute.dtu.dk:8082/");
	}

	public CustomerDTO createCustomer(String accountId) {
		CustomerDTO result = target
				.path("customers")
				.request(MediaType.APPLICATION_JSON)
				.post(Entity.text(accountId), CustomerDTO.class);
		return result;
	}
	
	public CustomerDTO getCustomer(String userId) {
		Response result = target
				.path("customers")
				.path(userId)
				.request(MediaType.APPLICATION_JSON)
				.get();
		if(result.getStatus()==404){
			return null;
		}
		return result.readEntity(CustomerDTO.class);
	}
	
	public boolean deleteCustomer(String userId) {
		Response response = target
				.path("customers")
				.path(userId)
				.request(MediaType.TEXT_PLAIN)
				.delete();

		return response.getStatusInfo().getFamily().equals(Status.Family.SUCCESSFUL);
	}

	public MerchantDTO createMerchant(String accountId) {
		MerchantDTO result = merchantTarget
				.path("merchants")
				.request(MediaType.APPLICATION_JSON)
				.post(Entity.text(accountId), MerchantDTO.class);

		return result;
	}
	
	public MerchantDTO getMerchant(String userId) {
		Response result = merchantTarget
				.path("merchants")
				.path(userId)
				.request(MediaType.APPLICATION_JSON)
				.get();
		if(result.getStatus() == 404){
			return null;
		}
		return result.readEntity(MerchantDTO.class);
	}
	
	public boolean deleteMerchant(String userId) {
		Response response = merchantTarget
				.path("merchants")
				.path(userId)
				.request(MediaType.TEXT_PLAIN)
				.delete();
		return response.getStatusInfo().getFamily().equals(Status.Family.SUCCESSFUL);
	}

	public String[] requestTokens(String userId, Integer amount) {
		Response response = target
				.path("customers")
				.path("requestTokens")
				.request(MediaType.APPLICATION_JSON)
				.post(Entity.json(new RequestTokensArgs(userId, amount)));
		
		if(response.getStatusInfo().getFamily().equals(Status.Family.SERVER_ERROR)) {
			return new String[0];
		}
		
		return response.readEntity(String[].class);
	}
	
	public TransactionDTO[] requestCustomerTransactionHistory(String userId) {
		TransactionDTO[] response = target
				.path("customers")
				.path(userId)
				.path("transactions")
				.request(MediaType.APPLICATION_JSON)
				.get(TransactionDTO[].class);
		return response;
	}
	
	public TransactionDTO[] requestMerchantTransactionHistory(String merchantId) {
	TransactionDTO[] response = merchantTarget
				.path("merchants")
				.path(merchantId)
				.path("transactions")
				.request(MediaType.APPLICATION_JSON)
				.get(TransactionDTO[].class);
		return response;
	}

	public boolean makePayment(String merchantId, BigDecimal amount, String tokenId) {
		Response response = merchantTarget.path("merchants")
				.path(merchantId)
				.path("transactions")
				.request(MediaType.TEXT_PLAIN)
				.post(Entity.json(new PaymentArgs(merchantId, amount, tokenId)));
		return response.getStatusInfo().getFamily().equals(Status.Family.SUCCESSFUL);
	}
}
