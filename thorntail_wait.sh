#!/bin/sh

# needs the argument corresponding to the container name
thorntail_wait()
{
    echo "Verifying $1 has started"
    docker logs $1 --follow | grep -q "Deployed"
}

thorntail_wait dtupay-customerservice
thorntail_wait dtupay-merchantservice
thorntail_wait dtupay-tokenservice
thorntail_wait dtupay-paymentservice
