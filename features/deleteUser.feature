Feature: Delete User
	Description: A user should be able to delete their account from the system
	Actors: Customer, Merchant
	
Scenario: A customer deletes their account
	Given a customer exists
	When the customer deletes their account
	Then the customers account is deleted
	
Scenario: A merchant deletes their account
	Given a merchant exists
	When the merchant deletes their account
	Then the merchant account is deleted
	
Scenario: An unknown customer deletes their account
	Given a customer does not exists
	When the customer deletes their account
	Then the deletion is rejected
	
Scenario: A unknown merchant deletes their account
	Given a merchant does not exists
	When the merchant deletes their account
	Then the deletion is rejected