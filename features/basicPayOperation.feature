@BankAccounts
Feature: Basic pay operation
	Description: When the merchant scans that token request a payment for a specific amount, then the payment succeeds and the correct amount is transfered from the customer to merchant account in the bank.
	Actors: Customer, Merchant, Bank

Scenario: Customer pays merchant
	Given the customer has bank account with 10 credits
	And the customer is registered with DTUPay
	And that the customer has 1 token
	Given the merchant has a bank account with 0 credits
	And the merchant is registered with DTUPay
	
	When the merchant request a payment for 10 credits
	Then 10 credits are transfered from the customer bank account to the merchant bank account

Scenario: Customer pays merchant without token
	Given the customer has bank account with 10 credits
	And the customer is registered with DTUPay
	And that the customer has 0 token
	Given the merchant has a bank account with 0 credits
	And the merchant is registered with DTUPay
	
	When the merchant request a payment for 10 credits
	Then 0 credits are transfered from the customer bank account to the merchant bank account
	
Scenario: Customer pays merchant with fake token
	Given the customer has bank account with 10 credits
	And the customer is registered with DTUPay
	And that the customer has a fake token
	Given the merchant has a bank account with 0 credits
	And the merchant is registered with DTUPay
	
	When the merchant request a payment for 10 credits
	Then 0 credits are transfered from the customer bank account to the merchant bank account
	
Scenario: Customer pays merchant with an already used token
	Given the customer has bank account with 10 credits
	And the customer is registered with DTUPay
	And that the customer has 1 token
	Given the merchant has a bank account with 0 credits
	And the merchant is registered with DTUPay
	
	When the merchant request a payment for 5 credits
	Then 5 credits are transfered from the customer bank account to the merchant bank account
	When the merchant request a payment for 5 credits
	Then 0 credits are transfered from the customer bank account to the merchant bank account
		
Scenario: Customer cannot afford payment
	Given the customer has bank account with 10 credits
	And the customer is registered with DTUPay
	And that the customer has 1 token
	Given the merchant has a bank account with 0 credits
	And the merchant is registered with DTUPay
	
	When the merchant request a payment for 20 credits
	Then 0 credits are transfered from the customer bank account to the merchant bank account
	
Scenario: Customer pays a fraud merchant with invalid bank account 
	Given the customer has bank account with 10 credits
	And the customer is registered with DTUPay
	And that the customer has 1 token
	
	Given the merchant has invalid bank account
	And the merchant is not registered with DTUPay
	
	When the merchant request a payment for 10 credits
	Then 0 credits are transfered from the customer bank account

Scenario: Negative payments are rejected
	Given the customer has bank account with 10 credits
	And the customer is registered with DTUPay
	And that the customer has 1 token
	Given the merchant has a bank account with 0 credits
	And the merchant is registered with DTUPay
	
	When the merchant request a payment for -10 credits
	Then 0 credits are transfered from the customer bank account to the merchant bank account