@TransactionHistory
Feature: Transaction history
	Actors: Customers, Merchants, Bank
	Description: Customers and merchants should be able to see their transaction history 
	
Background: 
	Given The customer has a bank account with a balance of 50 credits
	And the merchant has a bank account with a balance of 0 credits
	And the customer is registered at DTUPay
	And the merchant is registererd at DTUPay
	And the customer has 1 token
	
Scenario: Customer checks transaction history
	Given the customer has transfered 50 credits to the merchant
	When the customer requests their transaction history
	Then a transaction shows on their history
	And the amount of the transaction is 50
	And the reciever is the intended merchant
	And the status of the transaction is success
	And the token used is the same as the customer used
	
Scenario: Merchant checks transaction history
	Given the customer has transfered 50 credits to the merchant
	When the merchant requests their transaction history
	Then a transaction shows on their history
	And the token used is the same as the customer used
	And the amount of the transaction is 50
