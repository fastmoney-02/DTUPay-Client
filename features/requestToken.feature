Feature: Request token
	Description: User should be able to request 1 - 5 tokens if they only have 1 or 0 tokens currently
	Actors: User
	
Background: A user is registered
	Given a user exists
	
Scenario: User requests 1 token(s)
	Given the user has 0 tokens
	When the user requests 1 token(s)
	Then the user has 1 token(s)
	
Scenario: User requests 5 token(s)
	Given the user has 0 tokens
	When the user requests 5 token(s)
	Then the user has 5 token(s)
	
Scenario: User requests 4 token(s)
	Given the user has 1 tokens
	When the user requests 4 token(s)
	Then the user has 5 token(s)
	
Scenario: User requests 2 token(s)
	Given the user has 3 tokens
	When the user requests 2 token(s)
	Then the user has 3 token(s)
	
Scenario: User requests 6 token
	Given the user has 0 tokens
	When the user requests 6 token(s)
	Then the user has 0 token(s)
	
Scenario: User requests 0 token
	Given the user has 0 tokens
	When the user requests 0 token(s)
	Then the user has 0 token(s)